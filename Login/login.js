function validateForm() {
  var email = document.getElementById("email").value;
  if (!isValidEmail(email)) {
    document.getElementById("email").classList.add("error");
    return false;
  }
  return true;
}

function isValidEmail(email) {
  var regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
  return regex.test(email);
}